/*
Copyright © 2017-2018 Team Portaal
Authors: Josua Kiefner
Contact: fsportal@tutanota.de

Portaal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Portaal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Portaal.  If not, see https://www.gnu.org/licenses/">https://www.gnu.org/licenses/.
*/
var icons;
var iconOptionsIndex=0;
var editmodeState=0;
var placeholder;
var dilinkTarget;
var firstExtension;
var calcResult;
var dilinkDomains;
var dilinkIndex;
var settings;
var defaultSettings;
var actualInpuut;
var borderline="";
var searchEngines;
var keywords = [[["encodeuri"], "encodeuri(input)"],
[["decodeuri"], "decodeuri(input)"],
[["qr", "qrcode"], "qrCode(input)"],
[["stoppuhr", "stopwatch"], "stopwatch(input)"],//TODO
[["testdatei", "testfile"], "testfile(input)"],
[["passwort", "password"], "generatePassword(input)"],
[["tolowercase"], "tolowercase(input)"],
[["touppercase"], "touppercase(input)"],
[["wochentag", "dayofweek"], "dayOfWeek(input)"],
[["datum", "date"], "date(input)"],
[["kalenderwoche", "kw", "calendarweek"], "calendarWeek()"],
[["uhr", "zeit", "uhrzeit", "clock", "time"], "clock()"]
];
var split;
var splitwk;
var splitlc;
var inputwk;
var currencyRates="";
var availableCurrencies="";
var converterResult;
var size;
var actualTab=1;
var tabContent=[];
var tabSuggestions=[];
var favoriteCurrencies=["EUR", "USD", "GBP", "BTC"];
var currencyNames="";
var hours=0;
var minutes=0;
var seconds=0;
var milliseconds=0;
var stopwatchinterval;
var clickableIndex=0;
var actualClickableIndex=-1;
var inputSuggestions=[];
var currentHistoryEntry="";
var urlParameter = new Object();
var qrcodeSvg;
var notificationNumber=0;
var hiddenAreaNumber=0;
var qrcodeSvg;
hints=[["calc", "res/icons/calc.svg", ["25^2^+e*pi/floor(7,28)"]],
["websearch", "res/icons/search.svg", ["i18next.t('websearchExample')"]],
["directlink", "res/icons/Wikipedia.svg", ["wikipedia", "youtube", "facebook", "github"]],
["directlinkSearch", "res/icons/search.svg", ["wiki Richard Matthew Stallman", "i18next.t('directlinkSearchExample1')", "sof How to exit the Vim editor?", "i18next.t('directlinkSearchExample2')"]],
["converter", "res/icons/currency.svg", ["1,7BTC+500EUR-1700SEK+39SEK"]],
["algebra", "res/icons/algebra.svg", ["x", "1/(2*2^x^)+1,739"]],
["qrcode", "res/icons/qr.svg", ["`qr ${i18next.t('helloWorld')}`"]],
["password", "res/icons/key.svg", ["i18next.t('password')"]],
["colors", "res/icons/color-management.svg", ["#3daee9 salmon 51,221,170"]],
["date", "res/icons/calendar.svg", ["i18next.t('date')"]],
["testfile", "res/icons/textfile.svg", ["`${i18next.t('testfile')} 10mb`"]],
["dayOfWeek", "res/icons/calendar.svg", ["i18next.t('dayOfWeek')"]],
["url", "res/icons/websearch.svg", ["https://planet.kde.org", "https://mycroft.ai", "https://radiolise.gitlab.io"]]];
var currentHint;
var currentEditedIcon;
var settings={};
var lastInput;
var popstate;
var breezeColors=["#fcfcfc","#eff0f1","#4d4d4d","#31363b","#232629","#3daee9","#da4453","#ed1515","#f47750","#f67400","#fdbc4b","#c9ce3b","#1cdc9a","#2ecc71","#11d116","#1d9913"];


function get(url, code){
    var httpRequest = new XMLHttpRequest();
httpRequest.onreadystatechange = function(){
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
    if (httpRequest.status === 200) {
    data=httpRequest.responseText;
    code(data);
    }else{
    console.log(`Request to ${url} was not successful. Please check your internet connection.`);
    addNotification({content: `Request to ${url} was not successful. Please check your internet connection.`, selfdestroyingdelay: 5, symbolpath: "res/icons/dialog-error.svg"});
    }
    }
};
httpRequest.open('GET',url, true);
httpRequest.send();
}

function getComposedUrl(baseurl, suburl){
    if(/^((https?:\/\/)|(ftp:\/\/)|(file:\/\/\/))?([0-9-a-z]+\.)*[0-9-a-z]+\.[a-z][a-z]+(\:[0-9]{1,4})?(\/[0-9-._~a-z.]+)*\/?(\?.*)?(\#[^ %]*)?$/.test(suburl.toLowerCase())){
     return suburl   
    }else{
     return baseurl + suburl;
    }
}

function reloadPortaal(){
 openWindow({content: `${i18next.t("reloadMessage")}<br><button style="float: right" onclick="location.reload();">${i18next.t("reloadPortaal")}</button>`});
}

function loadLocalizedContent(){
    resize();
    e("aboutButton").title=i18next.t("about");
    e("settingsButton").title=i18next.t("settings");
    e("editButton").title=i18next.t("edit");
}

function closeLastWindow(){
    closeBox(e("windowDiv").lastChild);
}

function settingsWindow(){
    var languageOptions="";
    var languages=i18next.languages;
    languageOptions+=`<option value=''>${i18next.t("automatic")} (${i18next.language})</option>`
    for(var i=0; i<languages.length; i++){
        languageOptions+=`<option value='${languages[i]}'>${languages[i]}</option>`;
    }
    openWindow({content: `<p style="font-size: 2em">${i18next.t("settings")}</p>
<!--<label><input onchange="changeSetting(this.id, this.checked);" type="checkbox" id="linknewtab" >Links in neuem Tab öffnen.</label>
<br>
<br>-->
<button onclick="chooseSearchEngine(\`closeBox(this)\`);">${i18next.t("searchEngine")}</button>
<br>
<br>
<label>${i18next.t("language")}: <select id='language' value="" oninput="ssi('lang', this.value); setTimeout(reloadPortaal, 250);">
${languageOptions}
</select>
<br>
<br>` + getHiddenArea({content: `<label style="margin-left: 10px; background-color: white; padding-left: 2px; padding-right: 2px;">${i18next.t("settings")}</label>
 <fieldset style="margin-top: -10px; margin-right: 15px;">
 <textarea id="settingsText" style="background-color: white; resize: none; margin-top: 10px; width: calc(100%); height: 200px; border: none; background-color: #EFF0F1;" type="textbox"></textarea>
 <div style="text-align: right;"><i class="textButton" style="" onclick="importSettings(document.getElementById('settingsText').value);">${i18next.t("save")}</i></div>
 </fieldset>
<label style="margin-left: 10px; background-color: white; padding-left: 2px; padding-right: 2px;">${i18next.t("defaultSettings")}</label>
 <fieldset style="margin-top: -10px; margin-right: 15px;">
 <textarea id="defaultSettingsText" style="background-color: white; resize: none; margin-top: 10px; width: calc(100%); height: 200px; border: none; background-color: #EFF0F1;" type="textbox"></textarea>
 <div style="text-align: right;"</div>
 </fieldset>${sse(0)}`, moretext: "+ JSON", lesstext: "- JSON"}) + `<div style="height:0px;overflow:hidden">
   <input onchange="handleFileSelect(event);" type="file" id="import" name="fileInput" />
</div>
<br>
<div style="text-align: right;" class="hidden">
 <!--<i class="textButton" style="font-size: 0.75em;" onclick="restoreSettings();">Rückgängig</i>-->
 <button onclick="restoreDefaultSettings()">${i18next.t("resetDefaultSettings")}</button>
</div>

<p style="text-align: right; width: 100%;">
 <button style="margin-right: 10px;" onclick="document.getElementById('import').click();">${i18next.t("import")}</button>
 <button onclick="exportSettings();" style="">${i18next.t("export")}</button>
</p>
</div>`, "id": "settingsWindow"});
e("defaultSettingsText").value=(JSON.stringify(defaultSettings)!="null"?JSON.stringify(defaultSettings):"");
e("settingsText").value=(JSON.stringify(settings)!="null"?JSON.stringify(settings):"");
e("language").value=gsi("lang")||"";
}


function initSettingsCheckbox(id){
 e(id).checked=gsi(id);
 e(id).onclick=function(){ssi(this.id, this.checked);};
}

function chooseSearchEngine(options){
  openWindow({content:`<h2>${i18next.t("searchEngine")}</h2>
      <table style="width: 100%;">
  <tr>
  <td>${i18next.t("link")}</td>
    <td>
    <input autofocus  style="width: 100%;"  id="searchEngineUrl" placeholder="${i18next.t("urlToTheSite")}" value="` + `">
</td>
    </tr>
    <tr>
    <td>${i18next.t("icon")}</td>
    <td>
    <input  style="width: 100%;"  id="searchEngineIcon" placeholder="${i18next.t("urlToTheIcon")}" value="` + `">
    </td>
    </tr>
    </table>
    <button style="float: right; margin-top: 10px;" onclick="ssi('websearch', {url: e('searchEngineUrl').value, icon: e('searchEngineIcon').value}); e('chooseSearchEngine').remove(); ${options.saveaction||""}">${i18next.t("save")}</button>
    <br>
      <h3>${i18next.t("adviced")}:</h3>
      <table class="marginleft" style="vertical-align: middle;" id="recommendedSearchEngines"></table>
      <h3>${i18next.t("all")}:</h3>
      <table class="marginleft" style="vertical-align: middle;" id="searchEngines"></table>`, avoidduplicates: "chooseSearchEngine", id: "chooseSearchEngine"});
  if(gsi("websearch")){
      e('searchEngineUrl').value=gsi("websearch").url;
      e('searchEngineIcon').value=gsi("websearch").icon;
  }
  setTimeout(function(){
  for(var i in dilinkDomains){
   if(dilinkDomains[i].se=="true"){
       e("searchEngines").innerHTML+=getChooseSearchEngineEntryHtml({id: i, action: options.chooseaction});
   }else if(dilinkDomains[i].se=="recommended"){
        e("recommendedSearchEngines").innerHTML+=getChooseSearchEngineEntryHtml({id: i, action: options.chooseaction});
        e("searchEngines").innerHTML+=getChooseSearchEngineEntryHtml({id: i, action: options.chooseaction});
   }
  }
  },10);
}

function getChooseSearchEngineEntryHtml(options){
    var i=options.id;
    return `<tr class="hoverBackground">
<td style="cursor: pointer;" onclick="changeChooseSearchEngineInputs(` + i + `); ssi('websearch', {url: e('searchEngineUrl').value, icon: e('searchEngineIcon').value}); ${options.action||""}; "><img style="height: 1em; vertical-align: middle;" src='` + getSiteIconByIndex(i) + `'></td>
<td style="cursor: pointer; width: 100%;" onclick="changeChooseSearchEngineInputs(` + i + `); ssi('websearch', {url: e('searchEngineUrl').value, icon: e('searchEngineIcon').value}); ${options.action||""};">` +  dilinkDomains[i].t[0] + `</td>
<td><a href="${dilinkDomains[i].u}"style="text-decoration: none;"><img src="res/icons/emblem-symbolic-link.svg"></a></td>
<td><a href="https://${i18next.language.substring(0,2)}.wikipedia.org/wiki/Special:Search?search=` + dilinkDomains[i].t + `"><img src="res/icons/info.svg"></img></a></td>
</tr>`;
}

function changeChooseSearchEngineInputs(i){
    e("searchEngineUrl").value=dilinkDomains[i].u + dilinkDomains[i].s;
    e("searchEngineIcon").value=getSiteIconByIndex(i);
}

function getSettingsItem(name){
 if(settings){
 	if(settings[name]!=null){
 		return settings[name];
    }else{
	 return defaultSettings[name];
}
 }else{
	 return defaultSettings[name];
}
}

function gsi(name){
return getSettingsItem(name);
}

function setSettingsItem(name, value){
    if(settings==null){
     settings={};   
    }
    settings[name]=value;
saveSettings();
}

function ssi(name, value){
return setSettingsItem(name, value);
}

function showAllDilinks(){
var allDilinksHtml=`<h2 style="margin: 0;">${i18next.t("directlinks")}:</h2>
${i18next.t("total")}: <span id="numberOfDilinks"></span><div style="overflow-x: auto;"><table>`;
var numberOfDilinks=0;
for(var i in dilinkDomains){
allDilinksHtml+=`<tr>
<td><img src='` + getSiteIconByIndex(i) + `'></td>
<td>` + dilinkDomains[i].t[0] + `</td>
<td><a href="` + dilinkDomains[i].u + `">` + dilinkDomains[i].u + `</a></td>
</tr>`;
numberOfDilinks++;
}
allDilinksHtml+=`</table></div>`;
openWindow({content: allDilinksHtml, id: "allDilinks"});
e("numberOfDilinks").innerHTML=numberOfDilinks;
}

function showAllSearchDilinks(){
var allSearchDilinksHtml=`<h2 style="margin: 0;">${i18next.t("searchableDirectlinks")}:</h2>
${i18next.t("total")}: <span id="numberOfSearchDilinks"></span><div style="overflow-x: auto;"><table>`;
var numberOfSearchDilinks=0;
for(var i in dilinkDomains){
if(dilinkDomains[i].s){
allSearchDilinksHtml+=`<tr>
<td><img style="height: 1em" src='` + getSiteIconByIndex(i) + `'></td>
<td>` + dilinkDomains[i].t[0] + `</td>
<td><a href="` + dilinkDomains[i].u + `">` + dilinkDomains[i].u + `</a></td>
</tr>`;
numberOfSearchDilinks++;
}
}
allSearchDilinksHtml+=`</table></div>`;
openWindow({content: allSearchDilinksHtml, id: "allSearchDilinks"});
e("numberOfSearchDilinks").innerHTML=numberOfSearchDilinks;
}

function welcome(){
    var hiddenDescription=getHiddenArea({content: `
    ${((navigator.userAgent.indexOf("Firefox")!=-1)?`<button style="float: right;" onclick="window.external.AddSearchProvider('res/xml/opensearch.xml');">${i18next.t("addPortaalToYourBrowser")}</button><br>`:"")}
<div style="width: 100%;">${i18next.t("projectDescription")}<b>${i18next.t("contact")}:</b> ${i18next.t("contactInfo")}
<br><b>${i18next.t("attention")}:</b> ${i18next.t("developmentInfo")}
</div>`});
 addNotification({avoidduplicates: "welcomeMessage", id: "welcome", content: `<div style=""><div style="text-align: center">
<h2 style="margin-top: 0px; margin-bottom: 5px;">
    ${i18next.t("welcomeToPortaal")}</h2></div>
${i18next.t("portaalStatement")}<br>
</div>
${hiddenDescription}
     <label><input id="showWelcomeMessage" class="settingsInput" id="showWelcomeMessage" type="checkbox">${i18next.t("showWelcomeMessageOnStartup")}</label>
     <br>
     <button style="float: right;" onclick="e('welcome').remove(); showHints();">${i18next.t("introduction")}</button>
     `});
    initSettingsCheckbox("showWelcomeMessage");
//     ${getHiddenArea({content: `
//     ${((navigator.userAgent.indexOf("Firefox")!=-1)?`<button style="float: right;" onclick="window.external.AddSearchProvider('res/xml/opensearch.xml');">${i18next.t("addPortaalToYourBrowser")}</button><br>`:"")}
// <button style="float: right; margin-top: 10px;" onclick="e('welcome').remove(); showHints();">${i18next.t("introduction")}</button>${i18next.t("projectDescription")}})
}


function showHints(){
addNotification({id: "hints", content: `<h2 style="margin-bottom: 5px;" >${i18next.t("didYouAlreadyKnow")}</h2><div id="hintDiv"></div>
<div style="text-align: center; margin-top: 10px;">
<span id="currentHintDisplay"></span>/` + hints.length + `<br>
<button onclick="changeHint('previous');" style="float: left;">${i18next.t("previous")}</button>
<button onclick="changeHint('next')" style="float:right;">${i18next.t("next")}</button>
<button onclick="e('hints').remove(); showAllHints();">${i18next.t("totalOverview")}</button>
    </div>`});
changeHint(0);
}

function changeHint(i){
if(i=="next"){
if(currentHint+1<hints.length){
i=currentHint+1;
}else{
i=0;
}
}else if(i=="previous"){
if(currentHint-1>=0){
i=currentHint-1;
}else{
i=hints.length-1;
}
}
currentHint=i;
e("currentHintDisplay").innerHTML=i+1;
e("hintDiv").innerHTML=getHintHtml(i);
}

function getHintHtml(i){
    var html="";
    var html=`<img src="` + hints[i][1] + `" style="width: 50px; height: 50px; float: left">
<div style="min-height: 50px; margin-left: 60px;">
<span style="">` + i18next.t(hints[i][0]+"Hint") + `</span>`;
html+=`</div>`;
for(var j=0; j<hints[i][2].length; j++){
html+=`<div class="code" style="margin-top: 5px;">
` + (hints[i][2][j].match(/i18next\.t/)?eval(hints[i][2][j]):hints[i][2][j]) + '<button onclick="changeInput(`' + (hints[i][2][j].match(/i18next\.t/)?eval(hints[i][2][j]):hints[i][2][j]) + '`' + `);" style="float: right;">${i18next.t("tryIt")}</button></div>`;
}
return html;
}



function showAllHints(){
    var hintsHtml=`<div style="max-height: calc(100vh - 75px); overflow-y: auto;"><div style=""><div style="text-align: center">
<h2 style="margin-top: 0px; margin-bottom: 5px;">
    ${i18next.t("welcomeToPortaal")}</h2></div>
    ${i18next.t("portaalStatement")}<br>
    ${getHiddenArea({content: `<div style="width: 100%;">${i18next.t("projectDescription")}<br><b>${i18next.t("contact")}:</b> ${i18next.t("contactInfo")}
<br><b>${i18next.t("attention")}:</b> ${i18next.t("developmentInfo")}
</div>`})}
    <h2 style="margin-bottom: 5px;" >${i18next.t("didYouAlreadyKnow")}...</h2>`;
    var hr=""
    for(var i in hints){
        hintsHtml+=hr + getHintHtml(i);
        hr="<hr>";
    }
    hintsHtml+=`<div style="margin-top: 10px;text-align: center">
    <button onclick="e('allHints').remove(); welcome();">Einzelansicht</button>
    </div>
    </div>${sse(5)}`;
    e("windowDiv").innerHTML+=getBoxHtml({style: "float: right; z-index: 15; position: fixed; width: 100%; max-width: 400px; right: 0; top: 0; bottom: 0; box-shadow: 1px 1px 5px #e1e1e1; margin-right: 10px;", id: "allHints", content: hintsHtml, class: "box "});
}

function changeInput(input, addhistoryentry){
    cl("ahe: " + addhistoryentry);
if(addhistoryentry!=false){
    addHistoryEntry(input);
}
e('searchInput').value=input;
inputChange(input+"");
if(addhistoryentry!=false){
    addHistoryEntry(input);
}
}

//<p>Non-Profit</b>: Portaal wurde nicht geschaffen, um den größtmöglichsten Profit zu erzielen, sondern um den Nutzern auf eine innovative Art und Weise die Freiheit bei der Suche im Internet und vielem mehr zurückzubringen und gleichzeitig die globale Massenüberwachung zu minimieren. Portaal hat derzeit weder Einnahmen noch Ausgaben.


function jamendo(input){
if(/^(?:https+:\/\/)?(?:www.)?jamendo.com\/track\/\d{6}/.test(input)){
var id=input.match(/\d{6,7}/)[0];
addInputSuggestion("https://www.jamendo.com/Client/assets/toolkit/images/icon/favicon.1486494174817.ico", `<a href="https://mp3d.jamendo.com/download/track/${id}">mp3</a>${sse(9)}`);
}
}


//following function from developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/round.html
function round(number, precision) {
    var factor = Math.pow(10, precision);
    var tempNumber = number * factor;
    var roundedTempNumber = Math.round(tempNumber);
    return roundedTempNumber / factor;
};
var onlyColors;

function colors(input){

var colorPattern=`(#([0-9a-f]{3}){1,2})|(([0-9]{1,3}\,){2}[0-9]{1,3})|aliceblue|antiquewhite|aqua|aquamarine|azure|beige|bisque|black|blanchedalmond|blue|blueviolet|brown|burlywood|cadetblue|chartreuse|chocolate|coral|cornflowerblue|cornsilk|crimson|cyan|darkblue|darkcyan|darkgoldenrod|darkgray|darkgreen|darkgrey|darkkhaki|darkmagenta|darkolivegreen|darkorange|darkorchid|darkred|darksalmon|darkseagreen|darkslateblue|darkslategray|darkslategrey|darkturquoise|darkviolet|deeppink|deepskyblue|dimgray|dimgrey|dodgerblue|firebrick|floralwhite|forestgreen|fuchsia|gainsboro|ghostwhite|gold|goldenrod|gray|green|greenyellow|grey|honeydew|hotpink|indianred|indigo|ivory|khaki|lavender|lavenderblush|lawngreen|lemonchiffon|lightblue|lightcoral|lightcyan|lightgoldenrodyellow|lightgray|lightgreen|lightgrey|lightpink|lightsalmon|lightseagreen|lightskyblue|lightslategray|lightslategrey|lightsteelblue|lightyellow|lime|limegreen|linen|magenta|maroon|mediumaquamarine|mediumblue|mediumorchid|mediumpurple|mediumseagreen|mediumslateblue|mediumspringgreen|mediumturquoise|mediumvioletred|midnightblue|mintcream|mistyrose|moccasin|navajowhite|navy|oldlace|olive|olivedrab|orange|orangered|orchid|palegoldenrod|palegreen|paleturquoise|palevioletred|papayawhip|peachpuff|peru|pink|plum|powderblue|purple|rebeccapurple|red|rosybrown|royalblue|saddlebrown|salmon|sandybrown|seagreen|seashell|sienna|silver|skyblue|slateblue|slategray|slategrey|snow|springgreen|steelblue|tan|teal|thistle|tomato|turquoise|violet|wheat|white|whitesmoke|yellow|yellowgreen`;
var pattern=new RegExp("^" + colorPattern + "( " + colorPattern + ")*$");
if(pattern.test(input.trim().toLowerCase())){
try{
var colors=input.toLowerCase().trim().toLowerCase().split(/\s+/g);
// for(var i=0; i<colors.length; i++){
//     cl(i);
// if(pattern.test(colors[i])){
// if(onlyColors!=false){
// onlyColors=true;
// cl(true);
// }
// }else{
// onlyColors=false;
// cl(false);
// }
// }
// if(onlyColors==true){
var content="";
for(var i=0; i<colors.length; i++){
    var outputColors={};
var contentArray=[];
var hiddenContent="";
    //if(/^(#([0-9a-f]{3}){1,2})|(([0-9]{1,3}\,){2}[0-9]{1,3})$/.test(colors[i])){
    if(/^([0-9]{1,3}\,){2}[0-9]{1,3}$/.test(colors[i])){
        outputColors.hex="#" + convert.rgb.hex(eval("[" + colors[i] + "]"));
    outputColors.rgb=convert.hex.rgb(outputColors.hex);
    }else if(/^(#([0-9a-f]{3}){1,2})$/.test(colors[i])){
     outputColors.hex=colors[i];
    outputColors.rgb=convert.hex.rgb(colors[i]);
    }else{
        outputColors.rgb=convert.keyword.rgb(colors[i]);
        outputColors.hex="#" + convert.rgb.hex(outputColors.rgb);
    }
    
    outputColors.keyword = convert.rgb.keyword(outputColors.rgb);
    outputColors.hsl=convert.rgb.hsl(outputColors.rgb);
    outputColors.hsv=convert.rgb.hsv(outputColors.rgb);
    outputColors.hwb=convert.rgb.hwb(outputColors.rgb);
    outputColors.cmyk = convert.rgb.cmyk(outputColors.rgb);
    outputColors.ansi256 = convert.rgb.ansi256(outputColors.rgb);
    outputColors.ansi16 = convert.rgb.ansi16(outputColors.rgb);
    
    content+=`<div style='padding: 10px 10px 10px 10px; width: calc(100% - 20px); background-color: ` + outputColors.hex + `; min-height: 81px;'><div style="margin-left: calc(100% - 127.5px); padding: 2.5px 2.5px 2.5px 2.5px; background-color: white;  text-align: right; width: 125px;">`;
    
for(var key in outputColors){
    var contentArrayEntry="";
    if(outputColors[key]!=null){
        var color="";
    if(Array.isArray(outputColors[key])){
        color+=round(outputColors[key][0], 0);
     for(var j=1; j<outputColors[key].length; j++){
      color+="," + round(outputColors[key][j], 0);
     }
    }else{
        color=outputColors[key];
    }
    contentArrayEntry=`<span class="` + `" style="" title="`+ key + `">` + color + `<img src="res/icons/copy.svg" style="cursor: pointer; height: 1em; margin-left: 5px;" onclick="copyText('` + outputColors[key] + `');"><br></span>`;
    }
    contentArray.push(contentArrayEntry);
}

for(var c=0; c<=2; c++){
 content+=contentArray[c];   
}

for(var c=3; c<contentArray.length; c++){
 hiddenContent+=contentArray[c];   
}

hiddenContent+=`<span style="font-size: 11px; margin-top: -15px;">${i18next.t("colorValuesAreRounded")}</span>`;

content+=getHiddenArea({content: hiddenContent});
    content+=`</div></div>`;
    } 
//}
    addInputSuggestion("res/icons/color-management.svg", sse(6)+content, "", "colors", "");
}catch(ex){
cl("Colorvalues with errors and thus not parseable");
}
}
}

function getHiddenArea(values){
var id;
    if(values.id!=null){
 id=values.id;
}else{
 id="ha"+hiddenAreaNumber;   
hiddenAreaNumber++;
}
if(values.displaid==true){
more="buttonValue";
less="buttonText";
values.displaid="unset";
values.otherDisplaid="none"
}else{
more="buttonText";
less="buttonValue";
values.displaid="none";
values.otherDisplaid="unset"
}
    
if(values.moretext!=null){
values[more]=values.moretext;
}else{
values[more]=`+ ${i18next.t("more")}`;
}
if(values.lesstext!=null){
values[less]=values.lesstext;
}else{
values[less]=`- ${i18next.t("less")}`;
}

return `<button onclick="switchHiddenArea(this.id);" id="` + id + `Button" value="` + values.buttonValue + `" onclick="">` + values.buttonText + `</button>
<br>
<div value="` + values.otherDisplaid + `" style="display: ` + values.displaid + `;" id="` + id + `">
` + values.content + `
</div>`;
}

function switchHiddenArea(id){
value=e(id).innerHTML;
e(id).innerHTML=e(id).value;
e(id).value=value;
id=id.replace(/Button/, "");
if(e(id).style.display=="none"){
   e(id).style.display="unset"; 
}else{
    e(id).style.display="none";
}
}

function copyText(text){
    var searchInput=e("searchInput").value;
    e("body").innerHTML+=`<textarea id="textToCopy" style="height: 0; overflow-y: auto;">` + text + `</textarea>`;
        e("searchInput").value=searchInput;  
    e("textToCopy").select();
    document.execCommand('copy');
    e("textToCopy").remove();
    
}

//following function from https://stackoverflow.com/questions/3387427/remove-element-by-id

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

function urlModule(input){
    if(/^((https?:\/\/)|(ftp:\/\/)|(file:\/\/\/))?([0-9-a-z]+\.)*[0-9-a-z]+\.[a-z][a-z]+(\:[0-9]{1,4})?(\/[0-9-._~a-z.]+)*\/?(\?.*)?(\#[^ %]*)?$/.test(input.toLowerCase())){
        addInputSuggestion(getSiteIcon(input), input, "openUrl('" + input + "');", "urlSuggestion", "res/icons/websearch.svg");
}
}

function getSiteIcon(url){
    url=url.replace(/([^\/])\/[^\/].*/, "$1");
    if(/(https?:\/\/)|(ftp:\/\/)|(file:\/\/\/)/.test(url)==false){
     url="https://" + url;   
    }
    return url+"/favicon.ico";
}

function getSiteIconByIndex(index){
    if(dilinkDomains[index].i!=null){
        return getComposedUrl(dilinkDomains[index].url, dilinkDomains[index].i);
    }else{
     return dilinkDomains[index].u + "/favicon.ico";   
    }
}

function addNotification(options){
    options.style=(options.style||"") + "box-shadow: 1px 1px 5px #e1e1e1;";
options.class="box ";
e("notificationDiv").innerHTML= getBoxHtml(options) + e("notificationDiv").innerHTML;
}


function openWindow(options){
    //nearly all css from magnific-popup
	options.style=(options.style||"")+`position: relative; width: auto; max-width: 500px; margin: 20px auto;`;
    var id=options.id;
    options.id=options.id+"Container";
    options.closeaction=`closeBox(this);`;
    options.closeable=true;
	e("windowDiv").innerHTML+=`<div id="${id}" class="box ">
	<div onclick="closeBox(this);" style="z-index: 15; top: 0; left: 0; position: fixed; width: 100vw; height: 100vh; background: #4d4d4d; opacity: 0.5;"></div>
	
	<div onclick="closeBox(this);" style="text-align: center; position: absolute; width: 100%; height: 100%; left: 0; top: 0; padding: 0 8px; box-sizing: border-box; display: table;">
	<div style="width: 100%; position: relative; display: table-cell; vertical-align: middle; margin: 0 auto; z-index: 15; text-align: left;"><div style="position: relative; width: auto; max-width: 500px; margin: 0px auto;" onclick="(function(event){event.stopPropagation()})(event)"> ${getBoxHtml(options)}</div>
	</div></div></div>`;
}

function getBoxHtml(options){
     var stopselfdestroying="";
var avoidduplicates = "";
options.style=options.style||"";
if(options.avoidduplicates!=null){
var duplicates = document.getElementsByClassName(options.avoidduplicates + "Notification");
while(duplicates[0]){
duplicates[0].remove();
}
avoidduplicates = options.avoidduplicates + "Notification";
}
var id=options.id||notificationNumber;
notificationHtml=`<div id="${id}" class="boxStyling ${avoidduplicates} ${options.class||''}" style="${options.style||''}">`;
    if(options.selfdestroyingdelay!=null){
        var timeoutId;
	(function (notificationNumber) {
        timeoutId=window.setTimeout(function(){closeBox(e([notificationNumber]))}, options.selfdestroyingdelay*1000);
	})(notificationNumber);
    cl(timeoutId);
        stopselfdestroying=`window.clearTimeout(` + timeoutId + `);`
    }
    if(options.closeable!=false){
        notificationHtml+=`<img id="close" src="res/icons/close.svg" style="clickable: true; margin-left: 5px; width: 16px; float: right; cursor: pointer;" onclick="${options.closeaction||'closeBox(this);'}" ${stopselfdestroying}">`;
    }
    notificationHtml+='<table style="table-layout: fixed; width: calc(100% - 21px)">';
    if(options.symbolpath||options.symbol){
        notificationHtml+=`<td style="padding: 0; width: 25px; height: 25px; padding-right: 10px;"><div style="">`;
    if(options.symbolpath!=null){
        notificationHtml+='<img src="' + options.symbolpath + '" style="width: 25px; height: 25px;">';
    }else if(options.symbol!=null){
        switch(options.symbol){
            case "info":
                notificationHtml+='<img src="res/icons/info.svg" style="width: 25px; height: 25px;">';
                break;
                case "check":
                notificationHtml+='<img src="res/icons/check.svg" style="width: 25px; height: 25px;">';
                break;
                case "delete":
                notificationHtml+='<img src="res/icons/delete.svg" style="width: 25px; height: 25px;">';
                break;
    }
    }
    notificationHtml+=`</div></td>`;
    }
    notificationHtml+=`<td style="padding: 0; max-height: 100%;">`;
    if(options.content!=null){
        notificationHtml+=options.content;
        //TODO: overflow-y: auto
    }
    notificationNumber++;
    return notificationHtml + "</td></table></div></div>";
}


function closeBox(boxCloseButton){
    var boxElement=boxCloseButton;
    while(boxElement.className.indexOf("box ")==-1){
        boxElement=boxElement.parentElement;
    }
    boxElement.remove();
}

function init(){
    get("res/json/domains.json", function(data){
        dilinkDomains=JSON.parse((data).replace(/\n| /g,""));
    });
    loadSettings(function(){
        defaultSettings=JSON.parse(data); 
        cl("Default: " + defaultSettings);
        actualiseIcons();
    i18next.use(i18nextBrowserLanguageDetector)
    .use(i18nextXHRBackend)
    .init({
  debug: true,
  fallbackLng: ['en', 'de'],
  backend: {
      loadPath: 'res/locales/{{lng}}.json'
    }
}, function(err, t) {
    loadUrlParameter();
//     if(gsi("lastHistoryEntry")){
//         setUrlParameter({"hash": gsi("lastHistoryEntry"), "q":""});
//         ssi("lastHistoryEntry", "");
//         applyUrlParameter();
//     }
    loadLocalizedContent();
    if(getSettingsItem("showWelcomeMessage")!=false){
    setTimeout(welcome, 1000);
    }
});
    });

}

function addHistoryEntry(entry){
    entry=entry||e("searchInput").value;
    if(location.hash){
    /*if(decodeURI(location.hash.substring(1))!=entry){
        setUrlParameter({"hash": entry, "q": ""});
    }
    }*/if(urlParameter.q/*.replace(/\+/g, " ")*/!=entry){
        setUrlParameter({/*"hash": entry, */"q": entry});
    }
    }
}

function addHistoryEntryChecker(entry){
    entry=entry||e("searchInput").value;
    cl("checked");
//      historyLog.push([new Date().getTime(), entry]);
    if(lastInput&&lastInput[1]){
    if(new Date().getTime()-lastInput[0]>=3000&&lastInput[1]!=decodeURIComponent(urlParameter.q/*location.hash.substring(1)*/)){
        addHistoryEntry(entry);
    }
    }
    
//      if(historyLog[(historyLog.length - 2)]){
//     if(historyLog[historyLog.length-1][1].length<historyLog[historyLog.length-2][1].length){
//         if(currentHistoryEntry.indexOf(historyLog[historyLog.length-2][1])==-1){
//             if(historyLog[historyLog.length-1][0]-historyLog[historyLog.length-2][0]>1500){
//                 addHistoryEntry(historyLog[historyLog.length-2][1]);
//             }
//         }
//     }
// }
}

function loadUrlParameter(){
    cl("loadUrlParameter");
//     if(/\?/.test(window.location.href)){
//      urlParameterList=location.search.substring(1).split("&");//window.location.href.split("?")[1].split("#")[0].split("&");
     urlParameterList=location.hash.substring(1).split("&");
     urlParameter = new Object();
     for(var i=0; i<urlParameterList.length; i++){
      parameterSplit=urlParameterList[i].split("=");
      if(parameterSplit[0]!="q"){
            urlParameter[parameterSplit[0]]=decodeURIComponent(parameterSplit[1]);
      }else{
            urlParameter[parameterSplit[0]]=parameterSplit[1];
      }
//           urlParameter[parameterSplit[0]]=parameterSplit[1];
    }
    if(urlParameter.r){
//         urlParameter.q=urlParameter.q.replace(/\+/g, " ");
        urlParameter.q=urlParameter.q.replace(new RegExp("\\" + urlParameter.r, "g"), " ");
        delete urlParameter.r;
    }
    if(urlParameter.q){
        urlParameter.q=decodeURIComponent(urlParameter.q);
    }
//    }
//     if(/\#/.test(window.location.href)){
//     urlParameter.hash=decodeURI(window.location.href.split("#")[1]);
//     }
applyUrlParameter();
}


function applyUrlParameter(applyInput){
    cl("applyUrlParameter");
    if(applyInput!=false){
    if(urlParameter.q){
        changeInput(decodeURIComponent(urlParameter.q)||urlParameter.q, false);
    }
//     if(location.hash){
//         changeInput(decodeURI(location.hash.substring(1)), false);
//     }
    }
if(urlParameter.p){
loadSettings();
}
}



function setUrlParameter(parameter){
    parameterArray=Object.getOwnPropertyNames(parameter);
    for(var i=0; i<parameterArray.length; i++){
        if(parameterArray[i]){
        urlParameter[parameterArray[i]]=parameter[parameterArray[i]];
        }
    }
    
    urlParameterArray=Object.getOwnPropertyNames(urlParameter);
    urlParameterString="#";
    for(var i=0; i<urlParameterArray.length; i++){
        if(urlParameterArray[i]!="hash"&&urlParameter[urlParameterArray[i]]){
        urlParameterString+=urlParameterArray[i] + "=" + encodeURIComponent(urlParameter[urlParameterArray[i]]) + ((i<urlParameterArray.length-1)?"&":"");
        }
    }
    if(urlParameterString[urlParameterString.length-1]=="&"){
        urlParameterString=urlParameterString.substring(0, urlParameterString.length-1);
    }
    if(urlParameterString=="?=undefined"){
        urlParameterString="?"; //TODO: This is just a workaround
    }
//     if(urlParameter.hash){
//         urlParameterString+="#" + encodeURI(urlParameter.hash);
//     }
    history.pushState("", "", urlParameterString);
    var boolean=false;
    if(parameter.q==e("searchInput").value){
        boolean=true;
    }
    applyUrlParameter(false);
}

function resize(){
    if(window.innerWidth<444){
        e("searchInput").placeholder=i18next.t("search");
    }else{
        e("searchInput").placeholder=i18next.t("whatAreYouLookingFor");
    }
//     if(e("body").offsetHeight>(e("contentDiv").offsetHeight+70)){
//         e("contentDiv").style=`position: absolute; width: 100%; max-width: 580px; top:50%; left: 50%; transform: translate(-50%, -50%);`;   
//     }else{
//         e("contentDiv").style="display: table; width: 100%; max-width: 580px; margin: 35px auto 0 auto;";
//     }
}

//TODO
function clock(){
addInputSuggestion("res/icons/clock.svg", "<span id='hours'></span>:<span id='minutes'></span>:<span id='seconds'></span>");
var now=new Date();
e("hours").innerHTML=now.getHours();
e("minutes").innerHTML=now.getMinutes();
e("seconds").innerHTML=now.getSeconds();
}

//TODO
function calendarWeek(){
addInputSuggestion("res/icons/calendar.svg", "<div id='kws'></div>");
}

function getCalendarWeek(){
var now = new Date(2018, 1, 1);
var first = new Date(now.getFullYear(), 0, 1);
cl(now.getTime());
cl(first.getTime());
var day = first.getDay();
if(day==0){
day=7;
}
cl(day);
return (now.getTime()-first.getTime()+day*86400000)/604800000;
}

function date(input){
var now = new Date();
addInputSuggestion("res/icons/calendar.svg", now.toLocaleDateString(i18next.language));
}

function dayOfWeek(input){
var weekdays=[i18next.t("sunday"), i18next.t("monday"), i18next.t("tuesday"), i18next.t("wednesday"), i18next.t("thursday"), i18next.t("friday"), i18next.t("saturday")];
var now = new Date();
addInputSuggestion("res/icons/calendar.svg", weekdays[now.getDay()]+"<br>"+sse(8));
}

function tolowercase(input){
addInputSuggestion("", inputwk.toLowerCase());
}

function touppercase(input){
addInputSuggestion("", inputwk.toUpperCase());
}

function cl(text){
 console.log(text);   
}

function e(id){
 return document.getElementById(id);   
}

function generatePassword(input){
var amount;
var length;
if(/^[0-9]+$/.test(splitlc[1])){
amount=splitlc[1];
}else{
amount=10;
}
if(/^[0-9]+$/.test(splitlc[2])){
length=splitlc[2];
}else{
length=16;
}

var passwords=[];
var chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVXYZ0123456789@#€%&-+()*\"\':;!?/._,={}][\\$°|~";
var password="";
for(var i=0; i<amount; i++){
for(var j=0; j<length; j++){
    var randoom=random(0, (chars.length - 1));
password+=chars[randoom];
}
passwords.push(password);
password="";
}

var content="";
for(var i=0; i<amount; i++){
content+=passwords[i]+"<br>";
}
addInputSuggestion("res/icons/key.svg", content);
passwords=[];
password="";
}

function clearOnclick(){
 addHistoryEntry(e("searchInput").value);
 document.getElementById('inputSuggestions').innerHTML='';
 document.getElementById('searchInput').value='';
 document.getElementById('clearButton').style.display = 'none';
 e("clearButton").width='0px';
 document.getElementById('searchInput').focus();
 e('searchInput').style.width="calc(100% - 50px)";
}


// function highlightedSuggestionDown(){
//     if(inputSuggestions[actualClickableIndex+1]!=null){
//         changeHighlightedSuggestion(inputSuggestions[actualClickableIndex+1]);
//     }
// }
// 
// function highlightedSuggestionUp(){
//     if((actualClickableIndex-1)==-1){
//         if(actualClickableIndex!=-1){
//     document.getElementById(inputSuggestions[actualClickableIndex]).style.backgroundColor='';
//     }
//     actualClickableIndex=-1;
//     }
//     if(inputSuggestions[actualClickableIndex-1]!=null){
//         changeHighlightedSuggestion(inputSuggestions[actualClickableIndex-1]);
//     }
// }

function testfile(input){
    var content="";
    var onclick="";
    var id="";
    if(splitlc.length>1&&splitlc[1]!=""){
        var b="";
        if(splitlc[1].indexOf("b")==-1){
            b="b";
        }
        var content =  i18next.t("generateTestfileWith", {size: splitlc[1] + b});
        onclick = "generateTestfile('" + splitlc[1] + "');"
        id="testfileSuggestion";
    }else{
    content=i18next.t("pleasePutInTheSizeOfTheTestfile");
    }
    addInputSuggestion("res/icons/textfile.svg", sse(7)+content, onclick, id);
}

function generateTestfile(input){
    console.log("testfile is generated");
    var size=input.toLowerCase();
    size=size.replace(/b/g, "").replace(/,/g, ".").replace(/k/g, "*1024").replace(/m/g, "*1024*1024").replace(/g/g, "*1024*1024*1024").replace(/t/g, "*1024*1024*1024*1024").replace(/p/g, "*1024*1024*1024*1024*1024").replace(/e/g, "*1024*1024*1024*1024*1024*1024");
    size=eval(size);

newSize=size;
splitSize=[];
for(var i=27; i>0; i--){
if(newSize>Math.pow(2, i)){
splitSize[i]=(newSize-newSize%Math.pow(2, i))/Math.pow(2, i);
newSize=newSize-Math.pow(2, i)*splitSize[i];
}
}
z="0";
testFileContent=[];
    for(var i=0; i<27; i++){
z+=z;
if(splitSize[i+1]!=null){
for(var j=0; j<splitSize[i+1]; j++){
testFileContent.push(z);
}
}
}
z="";
for(var i=0; i<newSize; i++){
z+="0";
}
testFileContent.push(z);

var blob = new Blob(testFileContent, {type: ""});
    saveAs(blob, "testfile_" + size + "B.txt");
}

//TODO
function stopwatch(input){
    addInputSuggestion("res/icons/clock.svg", "<span id='hours'>00</span>:<span id='minutes'>00</span>:<span id='seconds'>00</span>:<span id='milliseconds'>00</span> <i style='float: right;' id='startstop' onclick='stopwatchStart();' class='textButton'>Start</i> <i style='float: right;' class='textButton'>Runde</i> <i style='float: right;' class='textButton' onclick='stopwatchStop(); hours=0; minutes=0; seconds=0; milliseconds=0; document.getElementById('hours').innerHTML='00'; document.getElementById('hours').innerHTML='00'; document.getElementById('minutes').innerHTML='00'; document.getElementById('milliseconds').innerHTML='00';>Reset</i><br><b>Achtung: Stoppuhr kann ungenau sein!</br>", "", "");
}

function stopwatchStart(){
    document.getElementById("startstop").innerHTML="Stop";
    document.getElementById("startstop").onclick=stopwatchStop;
    stopwatchinterval = setInterval(function(){
     milliseconds++;
     document.getElementById("milliseconds").innerHTML=milliseconds;
     if(milliseconds>=99){
      milliseconds=0;
      seconds++;
      document.getElementById("seconds").innerHTML=seconds;
      if(seconds>=60){
      seconds=0;
      minutes++;
      document.getElementById("minutes").innerHTML=seconds;
      if(minutes>=60){
           minuteseconds=0;
      hours++;
      document.getElementById("hours").innerHTML=hours;
      }
      }
     }
    }, 10);
}

function stopwatchStop(){
    console.log("stopwatchstop-event");
    document.getElementById("startstop").innerHTML="Start";
    document.getElementById("startstop").onclick=stopwatchStart;
    clearInterval(stopwatchinterval);
}

function random(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function qrCode(){
addInputSuggestion("res/icons/qr.svg", `<div id='qrcode'></div>
<button onclick="openQrcodeInNewTab();" id="fullscreenQr">${i18next.t("fullscreen")}</button>
<br>
<br>
<h3>${i18next.t("qrcode")}</h3>
<div class="marginleft">
<label>${i18next.t("errorCorrectionLevel")}:  
<select id='qrcodeErrorCorrectionLevel' oninput='regenerateQrcode();'>
<option value='L'>L (7%)</option>
<option value='M' selected>M (15%)</option>
<option value='Q'>Q (25%)</option>
<option value='H'>H (30%)</option>
</select></label>

<!--<label>${i18next.t("mode")}: 
<select id='qrcodeMode' oninput='regenerateQrcode();'>
<option value='Alphanumeric'>Alphanumeric</option>
<option value='Byte' selected>Byte</option>
<option value='Kanji'>Kanji</option>
<option value='Numeric'>Numeric</option>
</select></label>-->

<br>
<label>${i18next.t("size")}: <select id='qrcodeSize' oninput='regenerateQrcode();' value="5">

<option value='Automatic'>${i18next.t("automatic")}</option>
<option value='1'>1</option>
<option value='2'>2</option>
<option value='3'>3</option>
<option value='4'>4</option>
<option value='5' selected>5</option>
<option value='6'>6</option>
<option value='7'>7</option>
<option value='8'>8</option>
<option value='9'>9</option>
<option value='10'>10</option>
<option value='11'>11</option>
<option value='12'>12</option>
<option value='13'>13</option>
<option value='14'>14</option>
<option value='15'>15</option>
<option value='16'>16</option>
<option value='17'>17</option>
<option value='18'>18</option>
<option value='19'>19</option>
<option value='20'>20</option>
<option value='21'>21</option>
<option value='22'>22</option>
<option value='23'>23</option>
<option value='24'>24</option>
<option value='25'>25</option>
<option value='26'>26</option>
<option value='27'>27</option>
<option value='28'>28</option>
<option value='29'>29</option>
<option value='30'>30</option>
<option value='31'>31</option>
<option value='32'>32</option>
<option value='33'>33</option>
<option value='34'>34</option>
<option value='35'>35</option>
<option value='36'>36</option>
<option value='37'>37</option>
<option value='38'>38</option>
<option value='39'>39</option>
<option value='40'>40</option>
</select></label>

<br>
<label>${i18next.t("color")}: 
<input id='qrcodeFillcolor' type='color' oninput='restyleQrcode();' value='#000000'></label>
</div>

<h3>${i18next.t("background")}</h3>
<div class="marginleft">
<label>${i18next.t("color")}:
<input id='qrcodeBackgroundcolor' type='color' oninput='restyleQrcode();' value='#FFFFFF'></label><br>
</div>

<h3>${i18next.t("margin")}</h3>
<div class="marginleft">
<!--
<input id="qrcodeMarginActive" type="checkbox" value="false">
<div class="disabled">
<label>${i18next.t("color")}:
<input id='qrcodeBackgroundcolor' type='color' oninput='restyleQrcode();' value='#FFFFFF'></label><br>
-->
<label>${i18next.t("width")}:
<input id='qrcodeMarginWidth' type='number' oninput='restyleQrcode();' value='5' min="0"></label><br>
${sse(3)}
<!--
<label>${i18next.t("borderMarker")}
<input id='qrcodeBackgroundcolor' type='checkbox' oninput='restyleQrcode();' value='false'></label><br>
</div>
-->
</div>

<!--
<h3>${i18next.t("caption")}</h3>
<div class="marginleft">
<input type="checkbox" value="false">
<div class="disabled">
<label>${i18next.t("position")}:
<select id='qrcodeCaptionPosition' oninput='restyleQrcode();'>
<option value='Above'>${i18next.t("above")}</option>
<option value='Below'>${i18next.t("below")}</option>
</select>
</label>
<br>
<label>${i18next.t("text")}
<input disabled="true" id="qrcodeCaptionText">
</label>
</div>
<!--
height        
font size
font style
font type
</div>
-->
<h3>${i18next.t("print")}</h3>
<div class="marginleft">
<label>${i18next.t("width")} (${i18next.t("shareOfFullWidth")}):<input id="qrcodePrintWidth" type="number" min="0.1" max="1" step="0.1" value="1"></label>
<br>
<button onclick="openQrcodeInNewTab({code: 'window.print()', width: e('qrcodePrintWidth').value, center: false})">${i18next.t("print")}</button>
</div>

<h3>${i18next.t("save")}</h3>
<div class="marginleft">
<label>${i18next.t("resolution")}:
<input id="qrcodeSaveWidth" oninput="qrcodeSaveChangeResolution('width', this.value);" type="number" value="100" min="100">
X
<input id="qrcodeSaveHeight" oninput="qrcodeSaveChangeResolution('height', this.value);" type="number" value="100" min="100"></label>
<br>
<label>${i18next.t("quality")} (${i18next.t("onlyFor")} .jpeg): <input id="qrcodeSaveQuality" type="number" min="0" max="1" step="0.1" value="0.5"></label>
<br>
<button onclick='saveSvg(e("qrcodeSvg").outerHTML, inputwk, "svg", qrcodeSaveGetResolutionPoperty("width"), qrcodeSaveGetResolutionPoperty("height"));'>.svg</button>
<button class='textButton' onclick='saveSvg(e("qrcodeSvg").outerHTML, inputwk, "png", qrcodeSaveGetResolutionPoperty("width"), qrcodeSaveGetResolutionPoperty("height"));'>.png</button>
<button class='textButton' onclick='saveSvg(e("qrcodeSvg").outerHTML, inputwk, "jpeg", qrcodeSaveGetResolutionPoperty("width"), qrcodeSaveGetResolutionPoperty("height"), e("qrcodeSaveQuality").value);'>.jpeg</button>
</div>

`, "", "");
regenerateQrcode();
//window.setTimeout(function(){e("qrcodeCaption").value=inputwk;},0);   
}

function regenerateQrcode(){
  qrcodeSvg=generateQr(inputwk, "svg", e("qrcodeErrorCorrectionLevel").value, "Byte", e('qrcodeSize').value);
if(qrcodeSvg!="2"){
    qrcodeSvg=qrcodeSvg.replace(/<svg/, `<svg id="qrcodeSvg"`);
    qrcodeSvg=qrcodeSvg.replace(/<path/, `<path id="qrcodePath"`);
    restyleQrcode();
}else{
document.getElementById("qrcode").innerHTML="Text ist zu lang. Höchstens ~2331 Zeichen möglich.";
}
}

function restyleQrcode(){
e("qrcode").innerHTML=qrcodeSvg;
e("qrcodePath").outerHTML=e("qrcodePath").outerHTML.replace(/d="m0,0/, `d="m` + e("qrcodeMarginWidth").value + `,` + e("qrcodeMarginWidth").value);
//var newD=e("qrcodePath").getAttributeNS(null,"d").replace(/^m0,0/, "m" + e("qrcodeMarginWidth").value + "," + e("qrcodeMarginWidth").value);
//e("qrcodeSvg").setAttributeNS(null, "d", newD);
var width=2*e("qrcodeMarginWidth").value+34+8*e('qrcodeSize').value;
var height=width;
e("qrcodeSaveWidth").min=width;
e("qrcodeSaveWidth").value=width*10;
e("qrcodeSaveHeight").min=height;
e("qrcodeSaveHeight").value=height*10;
e("qrcodeSvg").setAttributeNS(null, "width", width);
e("qrcodeSvg").setAttributeNS(null, "height", width);
e("qrcodePath").setAttributeNS(null, "fill", "rgb(" + convert.hex.rgb(e("qrcodeFillcolor").value) + ")");
e("qrcodeSvg").innerHTML=`<rect fill="` + "rgb(" + convert.hex.rgb(e("qrcodeBackgroundcolor").value) + ")" + `" id="rect2" width="` + width + `" height="` + width + `" x="0" y="0"/>`+e("qrcodeSvg").innerHTML;
window.setTimeout(function(){var newD=e("qrcodePath").getAttributeNS(null,"d").replace(/^m0,0/, "m" + e("qrcodeMarginWidth").value + "," + e("qrcodeMarginWidth").value); e("qrcodePath").setAttributeNS(null, "d", newD);}, 1000);
// e("fullscreenQr").href="data:image/svg+xml," + e("qrcode").innerHTML;
}

function openQrcodeInNewTab(options){
    options=options||{};
    cl("width: " + options.width);
    options.width=options.width||1;
    cl("width: " + options.width);
    scaleFactor=((e("body").offsetWidth>e("body").offsetWidth)?e("body").offsetWidth:e("body").offsetHeight)/(2*e("qrcodeMarginWidth").value+34+8*e('qrcodeSize').value);
    if(options.width){
     scaleFactor=scaleFactor*options.width;   
    }
    var transformation=`position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); transform: scale( ${scaleFactor}, ${scaleFactor})`;
    //TODO: Left align/ not centered
//     if(options.center==false){
//     transformation=`position: absolute; top: ${50*options.width}%; left: ${50*options.width}%; transform: translate(${50*options.width}%, ${50*options.width}%) scale( ${scaleFactor}, ${scaleFactor})`;
//     }
    newTab=window.open();
    newTab.window.document.write(`<html>
${e("qrcode").innerHTML.replace(/id/, "style='" + transformation + "' id")}
    <script>
    ${options.code}
    </script>
</html>`);
}

var canvas;

function saveSvg(content, filename, type, width, height, quality){
type=type||"svg";
filename = filename||content.substring(0,50);
cl(quality);
quality=quality||0.5;
if(type=="svg"){
cl("width: " + width);
scaleFactor=width/(2*e("qrcodeMarginWidth").value+34+8*e('qrcodeSize').value);
cl("scaleFactor: " + scaleFactor);
    var blob = new Blob([content.replace(/id/, `style="transform: scale( ${scaleFactor}, ${scaleFactor});" id`)], {type: "img/svg"});
    // if(content.length<20){
    // }else{
    //      filename = content  .substring(0, 20); 
    // }
    saveAs(blob, filename + "." + type);
}else if(type=="png"||type=="jpeg"){
//     e("body").innerHTML+=`<canvas id="convertSvgToBitmapCanvas"></canvas><img id="convertSvgToBitmapImageSource" src="data:image/png;base64,` + new XMLSerializer().serializeToString(content) + `">`;
//     e("convertSvgToBitmapCanvas").getContext("2d").drawImage(e("convertSvgToBitmapImageSource"), 33, 71, 104, 124, 21, 20, 87, 104);
//     e("convertSvgToBitmapCanvas").remove();
    var mimeType="image/" + type;
    var img=document.createElement('img');
    canvas=document.createElement('canvas');
    img.style.width=width;
    img.style.height=height;
    canvas.width=width;
    canvas.height=height;
    img.src = 'data:image/svg+xml;base64,' + btoa(content);
    img.onload=function(){canvas.getContext('2d').drawImage(img, 0, 0, width, height); canvas.toBlob(function(blob){saveAs(blob, filename + "." + type);}, mimeType, quality)};
}
/*
width=1000;
height=1000;
filename="Hallo Welt!";
content=e("qrcodeSvg").outerHTML;
*/
}

function qrcodeSaveChangeResolution(property, value){
 var propertyToChange;
 var propertyNotToChange;
 var originalValue;
 var originalValuePropertyToChange;
    if(property=="width"){
     propertyNotToChange=e("qrcodeSaveWidth");
     propertyToChange=e("qrcodeSaveHeight");
     originalValue=e("qrcodeSvg").getAttribute("width");
     originalValuePropertyToChange=e("qrcodeSvg").getAttribute("height");
    }else if(property=="height"){
     propertyNotToChange=e("qrcodeSaveHeight");
     propertyToChange=e("qrcodeSaveWidth");
     originalValue=e("qrcodeSvg").getAttribute("height");
     originalValuePropertyToChange=e("qrcodeSvg").getAttribute("width");
 }
 if(e("qrcodeSvg").getAttribute("width")==e("qrcodeSvg").getAttribute("height")){
     propertyToChange.value=value;
 }else{
    propertyToChange.value=(value/originalValue)*originalValuePropertyToChange;
 }
}

function qrcodeSaveGetResolutionPoperty(property){
    if(property=="width"){
        propertyElement=e("qrcodeSaveWidth");
    }else if(property="height"){
        propertyElement=e("qrcodeSaveHeight");
    }
    if(propertyElement.value<propertyElement.min){
        propertyElement.value=propertyElement.min
 }
 return propertyElement.value;
}

function generateQr(content, format, errorCorrectionLevel, mode, size){
var stringToReturn = "";
    var length = content.length*8+12;
    var lengths =[0,128,224,352,512,688,864,992,1232,1456,1728,2032,2320,2672,2920,3320,3624,4056,4504,5016,5352,5712,6256,6880,7312,8000,8496,9024,9544,10136,10984,11640,12328,13048,13800,14496,15312,15936,16816,17728,18672];
    var i=1;
    var neededSize;
    while(length>lengths[i]){
     i++;   
    }
       neededSize=i;
       if(neededSize<41){
    
// var draw_qrcode = function(text, typeNumber, errorCorrectionLevel) {
//   document.write(create_qrcode(text, typeNumber, errorCorrectionLevel) );
// };

var create_qrcode = function(text, typeNumber, errorCorrectionLevel, mode,
    mb) {

  qrcode.stringToBytes = qrcode.stringToBytesFuncs[mb];

  var qr = qrcode(typeNumber || 4, errorCorrectionLevel || 'M');
  qr.addData(text, mode);
  qr.make();

if(format=="table"){
return qr.createTableTag();
}else if(format=="gif"){
return qr.createImgTag();
}else{
return qr.createSvgTag();
}
};

var update_qrcode = function() {
  var text = content.replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
  var t;
  if(size=="Automatic" || size==null){
      t = neededSize;
  }else{
      t = parseInt(size);   
  }
  qrcodeSize=t;
  var e = errorCorrectionLevel || "M";
  var m = mode || "Byte";
  var mb = "default";
stringToReturn = create_qrcode(text, t, e, m, mb);
};
update_qrcode();
// var lw=size*25;
// if(lw<75){
//  lw=75;   
// }else if(lw>550){
//  lw=550;   
// }
       }else{
stringToReturn = "2";           
       }
return stringToReturn;
}

function encodeuri(input){
    input=encodeURI(inputwk);
    addInputSuggestion("res/icons/development.svg", input, "changeInput(`" + input + "`);", "encodeuriSuggestion");
}

function decodeuri(input){
    input=decodeURI(inputwk);
    addInputSuggestion("res/icons/development.svg", input, "changeInput(`" + input + "`);", "decodeuriSuggestion");
}

function huiJo(input){
    if(shiftString(input.toLowerCase().replace(/ */g, ""), 1)=="ptufsfj"||shiftString(input.toLowerCase().replace(/ */g, ""), 1)=="fbtufsfhh"){
        if(!gsi("cyj")){
            addInputSuggestion("res/icons/tleoc.heavy.svg", shiftString(i18next.t("xLgwn"), -5));
            ucösp();
        }else{
            addInputSuggestion("res/icons/tleoc.heavy.svg", fodha());
        }
    
    }
    if(shiftString(input.toLowerCase().replace(/ */g, ""), 135)=="ìîî"||shiftString(input.toLowerCase().replace(/ */g, ""), 135)=="ìð"){
        addInputSuggestion("res/icons/emoji_u1f430.svg", sse(4));
    }
}

// function replaceInput(replacement){
//  document.getElementById("searchInput").value=replacement;
//  document.getElementById('inputSuggestions').innerHTML="";
//  document.getElementById("searchInput").focus();
//  inputChange(document.getElementById("searchInput").value);
// }

function importSettings(text){
    cl("Settings are imported.");
settings=JSON.parse(text);
saveSettings();
init();
}

function restoreDefaultSettings(){
localStorage.clear();
e('settingsWindow').remove();
init();
}

function openUrl(URL){
    if(/(https?:\/\/)|(ftp:\/\/)|(file:\/\/\/)/.test(URL)==false){
     URL="http://" + URL;   
    }
//     if(urlParameter.n=="false"){
//         if(urlParameter.q==e("searchInput").value){
//             window.location.href=URL;
//         }else{
//             window.open(URL, "_blank");
//         }
//     }else{
//     window.open(URL, "_blank");
//     }
cl("Para" + urlParameter.n);
    if(urlParameter.n=="false"){
        window.location.href=URL;
    }else if(urlParameter.n=="true"){
        window.open(URL, "_blank");
    }else{
        window.open(URL, "_blank");
    }
    //     if(getSetting("linknewtab")==false){
//         window.location.href=URL;
//     }else{
//         window.open(URL, "_blank");
//     }
}

function search(e){
    if(e){
        e.preventDefault();
    }
        // var i = actualClickableIndex;
// if(i==-1){
//  i=0;   
// }
// var i=0;
addHistoryEntry();
eval(firstExtension);
}

// TODO: Can be removed
// function submitInput(e){
// e.preventDefault();
// window[firstExtension]();
// }

function inputChange(input){
//actualClickableIndex=-1;
lastInput=[new Date().getTime(), e("searchInput").value];
    if(input){
     e('clearButton').style.display = 'inline';
     e('clearButton').style.width='25px';
     e("searchInput").style.width='calc(100% - 85px)';
 }else{
     e('clearButton').style.display = 'none';
     e('clearButton').style.width='0px';
     e("searchInput").style.width='calc(100% - 50px)';
 }

    clickableIndex=0;
    actualClickableIndex=-1;
    inputSuggestions=[];
    e('inputSuggestions').innerHTML="";
    borderline="";
    firstExtension="";
    actualInput=input;
    split=input.split(" "); //TODO: split is without keyword
    splitwk=split;
    splitwk.splice(0, 1);
    inputwk="";
spacing=0;
    huiJo(input);
    for(var i=0; i<splitwk.length; i++){                        
if(spacing==1){
inputwk+=" ";
}
spacing=1;
    inputwk+=splitwk[i];
    }
     splitlc=input.toLowerCase().split(" ");
    if(e("searchInput").value!=''){
     for(var i=0; i<keywords.length; i++){
        for(j=0; j<keywords[i][0].length; j++){
            //console.log(keywords[i][j]);
         if(keywords[i][0][j]==splitlc[0]){
          eval(keywords[i][1]);
         }
        }
    }
    
    dilink(input);
    urlModule(input);
    colors(input);
    calc(input);
    converter(input);
    websearchSuggestion();
    algebra(input);
    jamendo(input);
    //weather(input);
    }
    if(urlParameter.s=="true"&&popstate!=true){
        search();
        delete urlParameter.s;
    }
    if(urlParameter.n&&popstate!=true){
        delete urlParameter.n;
    }
}

function addInputSuggestion(icon, content, onclickCode, id, altImage){
    var onclick="";
    var style="";
    var mouseover="";
    var klass="";
    var name="";
    var idtag="";
    var altImg="";
    
if(onclickCode){
    onclick=`onclick="${onclickCode}" class='clickable'`;
    style="cursor: pointer; ";
    name="name='" + clickableIndex + "'";
    //mouseover=`onmouseover = "changeHighlightedSuggestion('` + id + `');"`
    clickableIndex++;
}
if(!firstExtension){
        firstExtension=onclickCode||"// do nothing";
}
if(id!=""){
    inputSuggestions.push(id);
    }

if(id!=""){
    idtag="id='" + id + "'";  
}
if(altImage!=""){
 altImg="onerror='this.src=`" + altImage + "`;'";   
}

//TODO: hoverBackground class="hoverBackground"
document.getElementById('inputSuggestions').innerHTML+=`<tr>
<td>
<div ` + idtag + " " + mouseover + " " + name + ` class="text ` + klass + `" style="padding-top: 10px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px; font-size: 15px;` + borderline + style + `"` +  onclick + `><table><td><img ` + altImg + ` style="height:25px; width: 25px; margin-right: 10px; margin-top: 0px; margin-bottom: 0px" src="` + icon + `"></img></td><td style="width: 100%">` + content + `</td></div>
</td>
</tr>`;
borderline="border-top: 1px solid #4d4d4d;";
}

function fodha(){
    var cyj=gsi("cyj");
    var amount=0;
    for(var i=0; i<cyj.length; i++){
        amount+=cyj[i];
    }
    if(amount<cyj.length){
    return shiftString(i18next.t("womvf", {frtzsy: shiftString(""+amount, 5)}), -5);
    }else{
        var serhöj="";
        for(var i=0; i<amount; i++){
            serhöj+=sse(-1, "", 25);
        }
        return `<div id="fodhaqwe"><div style="text-align: center;">
        <h2 style="padding: 0; margin: 0; margin-bottom: 10px;">${shiftString(i18next.t("cfioösdgm"), -5)}</h2>
        <img style="width: 100%; max-width: 150px; height: auto;" src="res/icons/emoji_u1f425.svg">
        </div>
        ${shiftString(i18next.t("kfjhs"), -5)}<br>
        ${serhöj}
        <br>
        <button onclick="ucösp();">${shiftString(i18next.t("vopejmnbh"), -5)}</button></div>`;
    }
}

function converter(input){
	if(/^[a-zA-Z0-9\/\*.,()+-]+$/.test(input)){
	success=0;
	if(success==0&&/^[a-zA-Z0-9.,()+-]+$/.test(input)&&/^[0-9()+-]+$/.test(input[0])&&!/^[0-9.,()+-]+$/.test(input)){
        addInputSuggestion("res/icons/currency.svg", `<table><tr><td id="tab1" onclick="switchTabActivity(1);" class="inactiveTab">${i18next.t("favorites")}</td><td id="tab2" onclick="switchTabActivity(2);" class="inactiveTab">${i18next.t("all")}</td><td style="text-align: right; border-bottom: 1px solid #4d4d4d; width: 500px"><a href="https://openexchangerates.com">openexchangerates</a> | ${i18next.t("version")}: ` + new Date(currencyRates.timestamp*1000).toLocaleString(i18next.language) + `</td></tr></table>
        <div style="padding-top: 5px;" id="currencyTableContent">${i18next.t("currenciesAreBeingLoaded")}...</div>`, "", "");
        tabContent[1]=`${i18next.t("currenciesAreBeingLoaded")}...`;
        tabContent[2]=`${i18next.t("currenciesAreBeingLoaded")}...`;
        switchTabActivity(1);
        error=false;
		if(currencyRates==""){
//         try{
        get("https://openexchangerates.org/api/latest.json?app_id=5bf8b5a2fa4645d3a4cbf2f2fad22be1&prettyprint=0", function(data){
            currencyRates=JSON.parse(data);
            get("https://openexchangerates.org/api/currencies.json?app_id=5bf8b5a2fa4645d3a4cbf2f2fad22be1&prettyprint=0", function(data){
                currencyNames=JSON.parse(data);
                convertUnits(input);
            });
        });
        //TODO: Offline support
        
//         }catch(e){
//             		currencyRates=JSON.parse(GET("res/json/currencies.json"));
//                     currencyNames=JSON.parse(GET("res/json/currencynames.json"));
// 
//         }
	}else{
        convertUnits(input);
    }
    }
    }
}

function convertUnits(input){
availableCurrencies=Object.getOwnPropertyNames(currencyRates.rates);
	for(var i=0; i<availableCurrencies.length; i++){             
			var re = new RegExp(availableCurrencies[i],"g");
			input=input.replace(re, "*(1/" + currencyRates.rates[availableCurrencies[i]] + ")");
	}
// 	input.replace(/,/g, ".");
	try{
	converterResult=eval(input);
    }catch(e){
        error=true;
    }
    if(error==false){
tabContent[1]=`${i18next.t("resultIsCalculated")}...`;
tabContent[2]=`${i18next.t("resultIsCalculated")}... `;
//actualiseTabViewContent();
tabContent[1]="<table>";
tabContent[2]=`<input oninput='filterCurrencies(this.value);' placeholder='${i18next.t("search")}' autofocus style='border: none; border-bottom: 1px solid #4d4d4d; font-size: 100%; width: 200px; background-color: white; padding-top: 5px; margin-bottom: 5px; margin-left: 5px;'>
<table id='allCurrenciesTable'>`;
for(var i=0; i<favoriteCurrencies.length; i++){
    tabContent[1]+=`<tr class='hoverBackground' id='converterSuggestion${i}' onclick='changeInput("${converterResult*currencyRates.rates[favoriteCurrencies[i]]}${favoriteCurrencies[i]}");' style='margin-bottom: 50px; cursor: pointer;' class='clickable'>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px;'>${favoriteCurrencies[i]}</td>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px;'>${converterResult*currencyRates.rates[favoriteCurrencies[i]]}</td>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px; width: 100%;'>${currencyNames[favoriteCurrencies[i]]}</td></tr>`;
    tabSuggestions[1]=favoriteCurrencies.length;
//     round(converterResult*currencyRates.rates[favoriteCurrencies[i]],2)).replace(/\./, ",")
}
tabContent[1]+="</table>";
for(var i=0; i<availableCurrencies.length; i++){
    tabContent[2]+=`<tr class='hoverBackground' id='converterSuggestion${i}' onclick='changeInput("${converterResult*currencyRates.rates[availableCurrencies[i]]}${availableCurrencies[i]}");' style='margin-bottom: 50px; cursor: pointer;' class='clickable'>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px;'>${availableCurrencies[i]}</td>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px;'>${converterResult*currencyRates.rates[availableCurrencies[i]]}</td>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px; width: 100%;'>${currencyNames[availableCurrencies[i]]}</td></tr>`;
    tabSuggestions[2]=availableCurrencies.length;
}
tabContent[2]+=`</table>${sse(1)}`;
actualiseTabViewContent();
	}
    }

function actualiseTabViewContent(){
    switchTabActivity(actualTab);
}

function switchTabActivity(id){
    actualTab=id;
    document.getElementById("tab" + id).className="activeTab";
    document.getElementById("currencyTableContent").innerHTML=tabContent[id];
    if(id==1){
     inactiveTabId=2;
    }else if(id==2){
      inactiveTabId=1;
    }
    document.getElementById("tab" + inactiveTabId).className="inactiveTab";
//     actualiseConverterSuggestions(tabSuggestions[id])
}

// function actualiseConverterSuggestions(n){
//     var converterInputSuggestions=[];
//     var beforeConverterInputSuggestions=[];
//     if(inputSuggestions.indexOf("converterSuggestion0")==-1){
// for(var i=0; i<n; i++){
//       inputSuggestions.push("converterSuggestion" + i);
//      }
//     }else{
//         while(inputSuggestions[0].indexOf("converterSuggestion")==-1){
//                 beforeConverterInputSuggestions.push(inputSuggestions[0]);
//                 inputSuggestions.shift();
//         }
//         while(inputSuggestions[0].indexOf("converterSuggestion")!=-1){
//             inputSuggestions.shift();
//         }
//         for (var i=0; i<n; i++){
//             converterInputSuggestions.push("converterSuggestion" + i);   
//         }
//         inputSuggestions=beforeConverterInputSuggestions.concat(converterInputSuggestions, inputSuggestions);
//     }
// }

function sse(i, width, height){
//     get("res/icons/tleoc.heavy.svg", function(data){
    if(gsi("cyj")){
        if(gsi("cyj")[i]!=1){
            var color1=random(0, breezeColors.length-1);
            var color2=random(0, breezeColors.length-1);
            while(Math.abs(color1-color2)<2){
                color2=random(0, breezeColors.length-1);
            }
            color1=breezeColors[color1];
            color2=breezeColors[color2];
            height=height||"115";
            height+="px";
            cl("height: " + height);
            width=width||"";
            if(width){
                width+="px";
            }
            return `<svg id="klegh${i}" style="position: relative; z-index: 15; ${i!=-1?"cursor: pointer;":""} transition: all 1s; top: 0; left: 0; width: ${width}; height: ${height}; padding: 5px;" ${i!=-1?`onclick="fjwsk(${i})"`:""} viewBox="0 0 100.033 133.7" preserveAspectRatio="xMinYMin meet" ><g xmlns="http://www.w3.org/2000/svg" inkscape:label="Ebene 1" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" inkscape:groupmode="layer" id="layer1" transform="translate(-38.113782,-59.247887)">
    <path style="fill:{{color1}};fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1.23857629;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" d="m 137.10257,151.47358 c -1.97626,10.62962 -8.81755,21.26065 -17.86774,29.23428 -9.05017,7.97363 -20.309229,12.19748 -31.120999,12.19749 -10.811784,1e-5 -22.070866,-4.22384 -31.121066,-12.19748 -9.0502,-7.97362 -15.891517,-18.60465 -17.86777,-29.23429 -6.362776,-34.22332 18.177892,-92.225712 48.988836,-92.225712 30.810949,2e-6 55.351509,58.002412 48.988739,92.225712 z" id="path4518" inkscape:connector-curvature="0" sodipodi:nodetypes="sssssss" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"/>
    <g id="g8678" style="fill:{{color1}};fill-opacity:1" transform="translate(-1.0753168e-4,1.1070736)">
      <path style="fill:{{color2}};fill-opacity:1;stroke:none;stroke-width:0.63981164px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="m 597.16211,-29.052734 -58.23047,70.041015 c -2.54231,15.397736 -4.1234,30.612748 -4.68555,45.355469 v 23.38867 c 0.1613,3.91513 0.41623,7.77442 0.7461,11.58399 l 62.16992,-74.779301 62.91797,75.677731 62.91601,-75.677731 62.91602,75.677731 62.91797,-75.677731 62.13476,74.738281 c 0.36164,-4.18471 0.62273,-8.43487 0.78125,-12.74609 V 87.425781 c -0.52448,-15.097608 -2.12428,-30.699894 -4.73437,-46.49414 L 848.83008,-29.052734 785.91211,46.625 722.99609,-29.052734 660.08008,46.625 Z" transform="matrix(0.26458333,0,0,0.26458333,-103.17372,112.64529)" id="path8676" inkscape:connector-curvature="0"/>
    </g>
  </g></svg>`.replace(/{{color1}}/g, color1).replace(/{{color2}}/g, color2);
        }else{
            return "";
        }
    }else{
        return "";
    }
//     });
}


function filterCurrencies(input){
    var amount=0;
    document.getElementById("allCurrenciesTable").innerHTML="";
    for(var i=0; i<availableCurrencies.length; i++){
    if(availableCurrencies[i].toUpperCase().indexOf(input.toUpperCase())!=-1 ||currencyNames[availableCurrencies[i]].toUpperCase().indexOf(input.toUpperCase())!=-1){
        e("allCurrenciesTable").innerHTML+=`<tr class='hoverBackground' id='converterSuggestion${i}' onclick='changeInput("${converterResult*currencyRates.rates[availableCurrencies[i]]}${availableCurrencies[i]}");' style='margin-bottom: 50px; cursor: pointer;' class='clickable'>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px;'>${availableCurrencies[i]}</td>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px;'>${converterResult*currencyRates.rates[availableCurrencies[i]]}</td>
    <td style='padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px; width: 100%;'>${currencyNames[availableCurrencies[i]]}</td></tr>`;
        amount++;
    }
}
// actualiseConverterSuggestions(amount);
}

function websearchSuggestion(){
    addInputSuggestion("res/icons/search.svg", actualInput, `websearch(encodeURIComponent('${actualInput}'));`);
}

function websearch(searchterm){
if(gsi("websearch")){
    openUrl(gsi("websearch").url.replace(/\[\[s\]\]/, searchterm));
}else{
    chooseSearchEngine({saveaction: `openUrl(gsi('websearch').url.replace(/\\[\\[s\\]\\]/, '${searchterm}'))`, chooseaction: `closeBox(this); openUrl(gsi('websearch').url.replace(/\\[\\[s\\]\\]/, '${searchterm}'))` });
}
}

function ucösp(){
    ssi("cyj", [0,0,0,0,0,0,0,0,0,0,0]);
    if(e("fodhaqwe")){
        e("fodhaqwe").parentElement.innerHTML=fodha();
    }
}

function agpl(){
get("res/agpl-3.0.html", function(data){
    openWindow({content: sse(10)+data});
});
}

function dilink(input){
if(/^[a-zA-Z- 0-9]+$/.test(splitlc[0])){
 var success=0;
 dilinkIndex=0;
    while(success<1){
        if(dilinkDomains[dilinkIndex].t==splitlc[0]){
url=dilinkDomains[dilinkIndex].u;
url=url.replace(/https:\/\//g, "").replace(/http:\/\//g, "").replace(/www./g, "");
if(inputwk.trim()&&dilinkDomains[dilinkIndex].s!=null){
         addInputSuggestion(getSiteIconByIndex(dilinkIndex), i18next.t("searchAtAfter", {site: url, term: inputwk}), "openUrl('"+ getComposedUrl(dilinkDomains[dilinkIndex].u, dilinkDomains[dilinkIndex].s.replace(/\[\[s\]\]/g, encodeURI(inputwk))) + "');", "dilinkSuggestion", "res/icons/websearch.svg");
}else{
addInputSuggestion(getSiteIconByIndex(dilinkIndex), url, "openUrl('"+ dilinkDomains[dilinkIndex].u + "');", "dilinkSuggestion", "res/icons/websearch.svg");
}
            success++;
        }
        if(dilinkIndex==(dilinkDomains.length-1)){
         success=1;
        }
        dilinkIndex++;
    }
}
}

function dilinkEvent(){
    window.location.href=(dilinkTarget);
}

function calcReplace(input){
    input=input.replace(/pi/g, "Math.PI");
    	input=input.replace(/e/g, "Math.E");
	input=input.replace(/round/g, "Math.round");
	input=input.replace(/root/g, "Math.sqrt");
	input=input.replace(/abs/g, "Math.abs");
    input=input.replace(/\|(.*)\|/g, 'Math.abs($1)');
	//input=input.replace(/ceil/g, "Math.ceil");
	input=input.replace(/floor/g, "Math.floor");
    	input=input.replace(/acos/g, "(180/Math.PI)*Math.acos");
	input=input.replace(/asin/g, "(180/Math.PI)*Math.asin");
	input=input.replace(/atan/g, "(180/Math.PI)*Math.atan");
    	input=input.replace(/[^a]cos\(/g, "Math.cos((Math.PI/180)*");
	input=input.replace(/[^a]sin\(/g, "Math.sin((Math.PI/180)*");
	input=input.replace(/[^a]tan\(/g, "Math.tan((Math.PI/180)*");
//     input=input.replace(/Math.aaccooss/g, "Math.acos");
//     input=input.replace(/Math.aassiinn/g, "Math.asin");
// 	input=input.replace(/Math.aattaann/g, "Math.atan");
	//input=input.replace(/min/g, "Math.min");
	//input=input.replace(/max/g, "Math.max");
	input=input.replace(/random/g, "Math.random()");
	input=input.replace(/,/g, ".");
    input=input.replace(/\[/g, "(");
    input=input.replace(/{/g, "(");
    input=input.replace(/]/g, ")");
    input=input.replace(/}/g, ")");
    input=input.replace(/([0-9]*)\^([^\^]*)\^/g, "Math.pow($1, $2)");
    input=input.replace(/([0-9]*)</g, "Math.pow($1,");
    input=input.replace(/>/g, ")");
    //console.log(input);
    return input;
}

function calcTest(input){
    var errorState=0;
try {
    (function(){
        eval(calcReplace(input));
    })()
}
catch(e) {
    errorState=1;
}
if(/^[a-z]+$/.test(input)){
errorState=1;		
}
if(input=="pi" || input=="e"){
errorState=0;	
}
return errorState;
}

function algebra(input){
if (/^[0-9\/\*.,()erandomminmaxsintanfloorceilabstroundcospih+-|<>]+$/.test(input)) {
calcTestResult=calcTest(input.replace(/x/g, "5"));                
input=calcReplace(input);
                algebraStart=-5;
                algebraEnd=5;
                algebraStepWidth=1;
                if(calcTestResult!=1 || input=="x"){
                addInputSuggestion("res/icons/algebra.svg", "<div style='margin-top: -20px; height:auto; width:auto' class='algebra-chart'>", "algebraEvent();", "");
                labelsArray=[];
                for(var i=algebraStart; i<=algebraEnd; i=i+algebraStepWidth){
                    labelsArray.push(i);
                }
                
                dataArray=[];
                for(var i=algebraStart; i<=algebraEnd; i=i+algebraStepWidth){
                    dataArray.push({x: i, y: eval(input.replace(/x/g, i))});
                }
                var chart = new Chartist.Line('.algebra-chart', {
  labels: labelsArray,
  series: [
{
      data: dataArray
  }
      ]
},  {
  plugins: [
    Chartist.plugins.tooltip()
  ]
});
}
}
}

function shiftString(text, distance){
    var shiftedText="";
    for(var i=0; i<text.length; i++){
        shiftedText+=String.fromCharCode(text.charCodeAt(i)+distance);
    }
    return shiftedText;
}

function calc(input){
if (/^[0-9\/\*.,()rooterandomminmaxsintanfloorceilabstroundcospih+-|<>]+$/.test(input)) {
input=input.replace(/x/g, "*");
	calcTestResult=calcTest(input);
input=calcReplace(input);
if(calcTestResult!=1 && input.indexOf("**") == -1){
calcResult=eval(input);
	if(calcResult.toString()!=document.getElementById("searchInput").value && !/[{]/.test(calcResult)){
    		html=document.getElementById('inputSuggestions').innerHTML;
    		addInputSuggestion("res/icons/calc.svg", calcResult, "changeInput(calcResult)", "calcSuggestion");
    		document.getElementById('inputSuggestions').innerHTML+=html;
     		firstExtension="changeInput(calcResult);";
	}
} 
}
}
 
function iconToLeft(index){
    if(index!=0){
	var icons=gsi("icons");
        placeholder=icons[index];
        icons[index]=icons[index-1];
        icons[index-1]=placeholder;
        iconOptionsIndex--;
	ssi("icons", icons);
    }
    actualiseIcons();
}

function fjwsk(i){
    var cyj=gsi("cyj");
    cyj[i]=1;
    ssi("cyj", cyj);
    addNotification({content: fodha(), selfdestroyingdelay: 5, symbolpath: "res/icons/tleoc.heavy.svg"});
    e(`klegh${i}`).style.cssText+="opacity: 0; top: -500px;";
    setTimeout(function(){e(`klegh${i}`).remove();}, 2000);
}

function iconToRight(index){
    if(index<(gsi("icons").length-1)){
	var icons=gsi("icons");
        placeholder=icons[index];
        icons[index]=icons[index+1];
        icons[index+1]=placeholder;
        iconOptionsIndex++;
	ssi("icons", icons);
    }
    actualiseIcons();
}

function removeIcon(){
var icons=gsi("icons");
icons.splice(iconOptionsIndex, 1);
setSettingsItem('icons', icons);
actualiseIcons();
e('editIconOptionsNotification').remove();
}
 
 function editmode(){
  if(editmodeState==0){
      editmodeState=1;
      e("editButton").src='res/icons/check.svg';
      e("editButton").style.width='50px';
      e("editButton").style.height='50px';
}else{
    editmodeState=0;
    setTimeout(function(){e("editButton").src='res/icons/edit.svg'}, 300);
    e("editButton").style.width='25px';
      e("editButton").style.height='25px';
}
    actualiseIcons();
}


function exportSettings(){
    var blob = new Blob([JSON.stringify(settings)], {type: "text/json;charset=utf-8"});
    saveAs(blob, "PortaalSettings.json");
}

function handleFileSelect(evt) {
    var files = evt.target.files;

    f = files[0];

    var reader = new FileReader();

    reader.onload = (function(theFile) {
        return function(e) {
        	importSettings(e.target.result);
        };
      })(f);
      reader.readAsText(f);
  }
  

function actualiseIcons(){
    e("icons").innerHTML="";

  if(editmodeState==1){
      for(var i=0; i<getSettingsItem("icons").length; i++){
          e("icons").innerHTML+=`<div title="${getSettingsItem("icons")[i][0]}" style="cursor: pointer; float: left; position: relative; width: 75px; height: 75px; margin: 20px 20px 20px 20px" onclick="editIconOptions(` + i + `)"><img style="position: absolute; top:50%; left: 50%;right: 50%; bottom: 50%; transform: translate(-50%, -50%); width: auto; height: auto; max-width: 75px; max-height: 75px;" src="` + getSettingsItem("icons")[i][1] + `"><img src="res/icons/preferences-vertical.svg" style="float: right; height: 30px; position: absolute; top:50%; right: 0%; transform: translate(0, -50%); margin-right: -20px; "></div>`;
      }
      e("icons").innerHTML+=`<div title="${i18next.t("addIcon")}" style="cursor: pointer; float: left; position: relative; width: 75px; height: 75px; margin: 20px 20px 20px 20px" onclick="editIconOptions(` + getSettingsItem("icons").length + `)"><img style="position: absolute; top:50%; left: 50%;right: 50%; bottom: 50%; transform: translate(-50%, -50%); width: 75px; height: 75px; max-width: 75px; max-height: 75px;" src="res/icons/add.svg"></div>${sse(2)}`;
  }else{
for(var i=0; i<getSettingsItem("icons").length; i++){
          e("icons").innerHTML+=`<div title="${getSettingsItem("icons")[i][0]}" style="cursor: pointer;  float: left; position: relative; width: 75px; height: 75px; margin: 20px 20px 20px 20px"><a style="z-index: auto;" href="` + getSettingsItem("icons")[i][0] + `"><img style="position: absolute; top:50%; left: 50%;right: 50%; bottom: 50%; transform: translate(-50%, -50%);  z-index: auto; max-width: 75px; max-height: 75px;" src="` + getSettingsItem("icons")[i][1] + `"/></a></div>`;
      }
      if(e("editIconOptionsNotification")){
          closeBox(e("editIconOptionsNotification"));
      }
}
}
// e("icons").innerHTML+=`<span><a onclick="iconOptionsIndex=icons.length;" href="#iconOptionsW" class="open-popup-link"><img src="res/icons/add.svg" class="ab" id="addIcon"></img></a></span>`;

function editIconOptions(index){
    var urlValue="";
    var iconValue="";
    if(index<getSettingsItem("icons").length){
     urlValue=getSettingsItem("icons")[index][0];
     iconValue=getSettingsItem("icons")[index][1];
    }
    iconOptionsIndex=index;
addNotification({id: "editIconOptionsNotification", avoidduplicates: "editIconOptionsNotification", content: `<div id="iconOptionsW">
 <table style="width: 100%;">
  <tr>
  <td>${i18next.t("link")}</td>
    <td>
    <input autofocus  style="width: 100%;"  id="iconOptionsW1" placeholder="${i18next.t("urlToTheSite")}" value="${urlValue}">
</td>
    </tr>
    <tr>
    <td>Icon</td>
    <td>
    <input autofocus  style="width: 100%;"  id="iconOptionsW2" placeholder="${i18next.t("urlToTheIcon")}" value="${iconValue}">
    </td>
    </tr>
    </table>
     <button style="float: right;" onclick="saveIconOptions();">${i18next.t("save")}</button>
     ${((index<getSettingsItem("icons").length)?`
     <br>
<div style="text-align:center">
<img src="res/icons/arrow-left.svg" id="leftIconOptionsW" onclick="iconToLeft(iconOptionsIndex);" style="padding-top: 7.5px; padding-bottom: 7.5px; height: 35px; width: 35px; cursor: pointer;">
<img src="res/icons/delete.svg" id="removeIconOptionsW" onclick="removeIcon(iconOptionsIndex);" style="padding-top: 7.5px; padding-bottom: 7.5px; height: 35px; width: 35px; cursor: pointer;">
<img src="res/icons/arrow-right.svg" id="rightIconOptionsW" onclick="iconToRight(iconOptionsIndex);" style="padding-top: 7.5px; padding-bottom: 7.5px; height: 35px; width: 35px; cursor: pointer;">`:"")}
</div>
 </div>`, symbolPath: "res/icons/edit.svg"});
}

function aboutWindow(){
openWindow({id: "aboutWindow", content: `<p style="font-size: 2em">${i18next.t("about")} Portaal</p>
<!--<p><b>P</b>ortaal <b>o</b>ffers <b>r</b>eliability, <b>t</b>ransparency, <b>a</b>ccessibility, <b>a</b>nonymity and, of course, <b>l</b>iberty</p>-->
<!--TODO-->
<button onclick="closeBox(this); showAllHints();">${i18next.t("projectOverview")}</button>
<br>
<button onclick="closeBox(this); welcome();">${i18next.t("welcomeMessage")}</button>
<br>
<button onclick="closeBox(this); showHints();">${i18next.t("didYouAlreadyKnow")}</button>
<br>
<button onclick='showAllDilinks();'>${i18next.t("listWithAllDirectlinks")}</button>
<br>
<button onclick='showAllSearchDilinks();' style='text-align: left;'>${i18next.t("listWithAllSearchableDirectlinks")}</button>
<p>
© 2017-2018 Team Portaal
</p>
<span style="float: left; transform: rotate(180deg); margin-top: 2.5px; margin-right: 5px;">©</span><p> 2017-2018 Team Portaal</p>
<p>${i18next.t("authors")}: Josua Kiefner</p>

<p>${i18next.t("sourceCodeGetFrom")}
</p>

<p>${i18next.t("contactInfo")}
</p>

<p>Portaal is free software: you can redistribute it and/or modify
it under the terms of the <button onclick="agpl();">GNU Affero General Public License</button> as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
</p>
<p>
Portaal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
</p>
<p>
You should have received a copy of the GNU Affero General Public License
along with Portaal.  If not, see <a target="_blank" href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.
</p>
<p>${i18next.t("usedProjects")}
</p>
<ul>
<li><a style="margin-right: 5px;" target="_blank" href="https://jquery.com/">jquery</a>(MIT; © jQuery Foundation)</li>
<li><a style="margin-right: 5px;" target="_blank" href="https://fonts.google.com/specimen/Lato">Lato Light</a>(Open Font License)</li> 
<li><a style="margin-right: 5px;" target="_blank" href="https://github.com/KDE/breeze-icons">Breeze Icons</a>(LGPL-2.1; © Contributors; Veränderungen am Quellcode vorgenommen)</li> 
<li><a style="margin-right: 5px;" target="_blank" href="http://dimsemenov.com/plugins/magnific-popup/">Magnific Popup</a>(MIT; © 2014-2016 Dmitry Semenov)</li> 
<li><a style="margin-right: 5px;" target="_blank" href="https://gionkunz.github.io/chartist-js/">Chartist</a>(MIT; © 2017 Gion Kunz)</li> 
<li><a style="margin-right: 5px;" target="_blank" href="https://github.com/gionkunz/chartist-plugin-tooltip">Chartist Plugin Tooltip</a>(MIT; © 2016 Markus Padourek)</li> 
<li><a style="margin-right: 5px;" target="_blank" href="https://github.com/eligrey/FileSaver.js/">FileSaver</a>(MIT; Copyright © 2016 Eli Grey)</li> 
</ul>

<p>${i18next.t("trademarkNotice")}
</p>
<p>The word 'QR Code' is registered trademark of DENSO WAVE INCORPORATED http://www.denso-wave.com/qrcode/faqpatent-e.html</p>`});
}

function getLocalStorage(name) {
return localStorage.getItem(name);
} 

function loadSettings(code) {
    console.log(code);
var storedSettings = getLocalStorage("settings");
if(storedSettings) {
settings=JSON.parse(storedSettings);
    }else{
     settings=null;   
//     	document.getElementById("settingsText").value=GET(settingsFile);
    }
var settingsFile;
if(!urlParameter.p){
  settingsFile="res/json/PortaalSettings.json";
 }else{
      settingsFile=urlParameter.p;
//       setUrlParameter({"p": ""});
 }
 
 cl(code);
 get(settingsFile, code);
}

function saveSettings(){
localStorage.setItem("settings", JSON.stringify(settings));
    if(e("settingsWindow")){
        e("defaultSettingsText").value=(JSON.stringify(
            defaultSettings)!="null"?JSON.stringify(defaultSettings):"");
        e("settingsText").value=(JSON.stringify(settings)!="null"?JSON.stringify(settings):"");
    }
}

function addIcon(){
getSettingsItem("icons").push([document.getElementById("addIconUrl").value, document.getElementById("addIconIconUrl").value]);
actualiseIcons();
}

function saveIconOptions(){
    icons=gsi("icons");
if(iconOptionsIndex>(icons.length-1)){
    icons.push([document.getElementById("iconOptionsW1").value, document.getElementById("iconOptionsW2").value]);
}else{
icons[iconOptionsIndex][0]=document.getElementById('iconOptionsW1').value;
icons[iconOptionsIndex][1]=document.getElementById('iconOptionsW2').value;
}
actualiseIcons();
ssi("icons", icons);
e('editIconOptionsNotification').remove();
}

window.addEventListener("keydown", function (event) {
  if (event.defaultPrevented) {
    return; // Should do nothing if the key event was already consumed.
  }

  switch (event.key) {
    case "Escape":
      closeLastWindow();
      break;
//     case "ArrowUp":
// cl("ArrowUp");
// break;
    case "Enter":
         if(e("searchInput")==document.activeElement){
          search(event);   
         }else{
          return;   
         }
//         var i = actualClickableIndex;
//         cl("Entered");
// if(i==-1){dghsj
//  i=0;   
// }
// addHistoryEntry(e("searchInput").value);
    break;
    default:
      return;
  }
  //event.preventDefault();
}, true);
window.addEventListener('DOMContentLoaded', function(event){init();}, false);
window.addEventListener("warning", function(event){alert();}, true);
window.addEventListener("log", function(event){alert();}, true);
window.addEventListener("resize", function(){resize();}, true);
window.addEventListener("popstate", function(){cl("POPSTATE"); popstate=true; loadUrlParameter()}, true);
// window.onbeforeunload = function(e){ addHistoryEntryChecker();
//     ssi("lastHistoryEntry", historyLog[historyLog.length-1][1]);
// }
setInterval(addHistoryEntryChecker, 500);

// document.querySelector(".settingsInput").addEventListener('load', function(){
// cl("element loaded");
// }, true);
